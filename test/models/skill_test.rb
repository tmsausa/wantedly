require 'test_helper'

class SkillTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @skill = @user.skills.build(content: "Kind", count: 5)
  end
    
  test "should be valid" do
    assert @skill.valid?
  end
  
  test "user id should be present" do
    @skill.user_id = nil
    assert_not @skill.valid?
  end
  
  test "content should be present" do
    @skill.content = "   "
    assert_not @skill.valid?
  end
  
  test "skill should be at most 20 characters" do
    @skill.content = "a" * 21
    assert_not @skill.valid?
  end
  
  test "order should be the largest count first" do
    assert_equal skills(:ml), Skill.first
  end
end
