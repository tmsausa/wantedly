require 'test_helper'

class SkillsControllerTest < ActionController::TestCase
  
  def setup
    @skill = skills(:ml)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Skill.count' do
      post :create, skill: { content: "Lorem ipsum", count: 1 }
    end
    assert_redirected_to login_url
  end
  
  # test "should redirect update when not logged in" do
  #   patch :update, skill: { content: "Lorem ipsum", count: 1 }
  #   assert flash.empty?
  #   assert_redirected_to login_url
  # end
end
