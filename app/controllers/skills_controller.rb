class SkillsController < ApplicationController
  before_action :logged_in_user, only: [:create, :update]
  before_action :correct_user,   only: :destroy
  
  def create
    @user = User.find_by(id: params[:skill][:user_id]) 
    @skill = @user.skills.build(skill_params)
    if @skill.save
      flash[:success] = "A new skill created!"
      redirect_to @user
    else
      render 'static_pages/home'
    end
  end
  
  # エラーが起こる．ボタンを押した時の挙動なので
  # 11章のMicropostのデリートや12章のフォローボタンが参考になりそう
  def update 
    @user = User.find_by(id: params[:user_id])
    skill = Skill.find(params[:id])
    if skill.update_attribute(:count, params[:count])
      flash[:success] = "Skill updated"
      redirect_to request.referrer || root_url
    else
      flash[:error] = "Failure"
      redirect_to request.referrer || root_url
    end
  end
  
  def destroy
    @skill.destroy
    flash[:success] = "Skill deleted"
    redirect_to request.referrer || root_url
  end
  
  private
  
    def skill_params
      params.require(:skill).permit(:content, :count)
    end
    
    def correct_user
      @skill = current_user.skills.find_by(id: params[:id])
      redirect_to root_url if @skill.nil?
    end
end