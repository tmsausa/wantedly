class Skill < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(count: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 20 } #, uniquness: true を追加すると1つになってしまう．
  
end
