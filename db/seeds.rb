User.create!(name: "Example User",
             email: "example@railstutorial.org",
             password: "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
content_array = ["C", "Python", "Unity", "Robotics", "Rails", "JavaScript"]
users.each { |user| user.skills.create!(content: content_array[0],
                                          count: rand(0..50)) }
users.each { |user| user.skills.create!(content: content_array[1],
                                          count: rand(0..50)) }
users.each { |user| user.skills.create!(content: content_array[2],
                                          count: rand(0..50)) }
users.each { |user| user.skills.create!(content: content_array[3],
                                          count: rand(0..50)) }
users.each { |user| user.skills.create!(content: content_array[4],
                                          count: rand(0..50)) }
users.each { |user| user.skills.create!(content: content_array[5],
                                          count: rand(0..50)) }