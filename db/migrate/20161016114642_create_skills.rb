class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :content
      t.string :text
      t.references :user, index: true, foreign_key: true
      t.integer :count

      t.timestamps null: false
    end
  end
end
